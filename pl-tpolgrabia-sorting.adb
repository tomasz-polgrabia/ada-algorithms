with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Numerics.Discrete_Random;

package body Pl.Tpolgrabia.Sorting is

    function isSorted(arr: ElementArr) return Boolean is
    begin
        CheckSortedLoop:
        for i in Integer range arr'First .. arr'Last loop
            if arr(i) > arr(i-1) then
                return false;
            end if;
        end loop CheckSortedLoop;
        return true;
    end isSorted;

    procedure SelectSort(arr: in out ElementArr) is
        temp: Element;
        maxIdx: Integer;
    begin

        ILoop:
        for i in arr'Range loop
            maxIdx := i;

            JLoop:
            for j in Integer range i+1 .. arr'Last loop
                if arr(j) < arr(maxIdx) then
                    maxIdx := j;
                end if;
            end loop JLoop;

            temp := arr(i);
            arr(i) := arr(maxIdx);
            arr(maxIdx) := temp;

        end loop ILoop;

    end SelectSort;

    procedure InsertionSort(arr: in out ElementArr) 
    is 
        temp: Element;
        left, right, currIdx, placeToInsert: Integer;
    begin
        ILoop:
        for i in arr'First + 1 .. arr'Last loop

            if arr(i-1) >  arr(i) then

                left := arr'First;
                right := i - 1;

                JBinarySearchLoop:
                while left < right loop
                    currIdx := (left + right) / 2;
                    if arr(currIdx) < arr(i) then
                        left := currIdx+1;
                        if left > right then
                            left := right;
                        end if;
                    elsif arr(currIdx) = arr(i) then
                        left := currIdx;
                        right := currIdx;
                    else
                        right := currIdx-1;
                        if right < left then
                            right := left;
                        end if;
                    end if;

                end loop JBinarySearchLoop;

                -- right to idx, gdzie mamy wstawic element z i
            
                temp := arr(i);
                placeToInsert := right;

                if (arr(right) < temp) then
                    placeToInsert := right + 1;
                else
                    placeToInsert := right;
                end if;


                for j in reverse Integer range placeToInsert .. i - 1 loop
                    arr(j+1) := arr(j); -- przesuwamy o 1 w prawo
                end loop;

                arr(placeToInsert) := temp;

            end if;
            
        end loop ILoop;
    end InsertionSort;

    procedure BubbleSort(arr: in out ElementArr) is
        temp: Element;
    begin
        ILoop:
        for i in reverse Integer range arr'First .. arr'Last - 1 loop
            JLoop:
            for j in Integer range arr'First .. i loop
                if arr(j) > arr(j+1) then
                    temp := arr(j);
                    arr(j) := arr(j+1);
                    arr(j+1) := temp;
                end if;
            end loop JLoop;
        end loop ILoop;
    end BubbleSort;

    procedure QuickSortPart(arr: in out ElementArr; from, to: Integer) is 
        pivot,temp: Element;
        pivotIdx: Integer;
        i,j: Integer;
    begin

        if from >= to then
            return;
        end if;

        pivotIdx := (from+to) / 2;
        pivot := arr(pivotIdx);

        i := from;
        j := to;

        PartitionLoop:
        while i < j loop
            
            SearchForGreaterThanPivotLoop:
            while i < j and arr(i) < pivot loop
                i := i + 1;
            end loop SearchForGreaterThanPivotLoop;

            SearchForLesserThanPivotLoop:
            while i < j and arr(j) >= pivot loop
                j := j - 1;
            end loop SearchForLesserThanPivotLoop;

            if i < j and arr(i) >= pivot and arr(j) < pivot then

                if arr(i) = pivot then
                    pivotIdx := j;
                end if;

                temp := arr(i);
                arr(i) := arr(j);
                arr(j) := temp;

                i := i + 1;
                j := j - 1;
            end if;

        end loop PartitionLoop;

        if arr(i) < pivot then
            i := i + 1;
        end if;

        arr(pivotIdx) := arr(i);
        arr(i) := pivot;

        -- spartycjonowano
        
        QuickSortPart(arr, from, i-1);
        QuickSortPart(arr, i+1, to);

        null;
    end QuickSortPart;

    procedure QuickSort(arr: in out ElementArr) is 
--        temp: Element;
    begin
        QuickSortPart(arr, arr'First, arr'Last);

--        temp := arr(arr'First);
--        arr(arr'First) := arr(arr'Last);
--        arr(arr'Last) := temp;

    end QuickSort;

end Pl.Tpolgrabia.Sorting;
