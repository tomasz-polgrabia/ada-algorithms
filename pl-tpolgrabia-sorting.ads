generic
    type Element is private;
    type ElementArr is array (Positive range <>) of Element;
    with function "<" (X,Y: Element) return Boolean is <>; 
    with function ">" (X,Y: Element) return Boolean is <>;
    with function ">=" (X,Y: Element) return Boolean is <>;


package Pl.Tpolgrabia.Sorting is     
    procedure SelectSort(arr: in out ElementArr) with
        Post => isSorted(arr);
    procedure InsertionSort(arr: in out ElementArr) with
        Post => isSorted(arr);
    procedure BubbleSort(arr: in out ElementArr) with
        Post => isSorted(arr);
    procedure QuickSort(arr: in out ElementArr) with
        Post => isSorted(arr);

    function isSorted(arr: ElementArr) return boolean;

end Pl.Tpolgrabia.Sorting;
