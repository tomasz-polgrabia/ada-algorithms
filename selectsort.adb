with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Numerics.Discrete_Random;
with Pl.Tpolgrabia.Sorting;

procedure SelectSort is

    subtype RandRange is Integer'Base range 0 .. 256;
    type IntegerArray is array (Positive range <>) of Integer;

    package RandInteger is new Ada.Numerics.Discrete_Random(RandRange);

    package T_IO renames Ada.Text_IO;
    package I_IO renames Ada.Integer_Text_IO;


    tab: IntegerArray(1 .. 16);
    seed: RandInteger.Generator;

--    procedure IntSelectSort is new Pl.Tpolgrabia.Sorting.SelectSort (
--        Element => Integer,
--        ElementArr => IntegerArray
--    );
   
    package IntegerSorting is new Pl.Tpolgrabia.Sorting (
        Element => Integer,
        ElementArr => IntegerArray
    );
begin

    RandInteger.Reset(seed);

    RandFill:
    for idx in tab'Range loop
        tab(idx) := RandInteger.Random(seed);
        T_IO.Put("Element to: ");
        I_IO.Put(Item => tab(idx), Width => 1);
        T_IO.New_Line;
    end loop RandFill;

    T_IO.Put_Line("End of display");
    T_IO.New_Line;

--    IntSelectSort(tab);
--    IntegerSorting.SelectSort(tab);
--    IntegerSorting.InsertionSort(tab);
--    IntegerSorting.BubbleSort(tab);
    IntegerSorting.QuickSort(tab);

    DisplayLoop:
    for idx in tab'Range loop
        T_IO.Put("Element to: ");
        I_IO.Put(Item => tab(idx), Width => 1);
        T_IO.New_Line;
       
    end loop DisplayLoop;

    T_IO.Put_Line("End of display");
    T_IO.New_Line;

    

end SelectSort;
